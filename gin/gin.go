// Go Skeleton API.
//
// Sample REST API.
//
//     Schemes: http
//     Host: localhost:8081
//     BasePath: /
//     Version: v1
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
//
// swagger:meta
package gin

import (
	"fmt"

	"github.com/carantunes/go-skeleton/domain"
	"github.com/carantunes/go-skeleton/gin/handlers"
	"github.com/carantunes/go-skeleton/gin/handlers/middleware"
	"github.com/carantunes/go-skeleton/gin/handlers/v1/search"
	"github.com/gin-gonic/gin"
)

// APIService is a Gin API service
type APIService struct {
	*gin.Engine

	addr string
}

// New creates an initialized Gin APIService
func New(
	host,
	port,
	corsDomain,
	mode string,
	controllerAggregator domain.ControllerAggregate,
) (
	APIService,
	error,
) {
	loggerMiddleware, err := middleware.Logger()
	if err != nil {
		return APIService{}, err
	}

	gin.SetMode(mode)
	router := gin.New()
	router.Use(middleware.Cors(corsDomain), loggerMiddleware)

	searchHandler := search.NewHandler(controllerAggregator.ParamTranslator)

	router.GET("/ping", handlers.Ping())

	gv1 := router.Group("/v1")
	{
		gv1.POST("search/json", searchHandler.RedirectSearchJson())
		gv1.POST("search", searchHandler.RedirectSearch())

	}

	service := APIService{
		Engine: router,
		addr:   fmt.Sprintf("%s:%s", host, port),
	}

	return service, nil
}

// Start serves the API
func (api APIService) Start() error {
	return api.Run(api.addr)
}
