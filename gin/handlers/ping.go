package handlers

import (
	"net/http"

	"github.com/gin-gonic/gin"
)


// Pong response
// swagger:response pongResponse
type pongResponse struct {
	// in:body
	Body struct {
		// Pong message
		// example: pong
		Message string `json:"message"`
	}
}

// swagger:route GET /ping generic ping
//
// Ping allows to perform health check
//
//     Responses:
//       200: pongResponse
func Ping() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		resp := pongResponse{}
		resp.Body.Message = "pong"

		ctx.JSON(http.StatusOK, resp.Body)
	}
}