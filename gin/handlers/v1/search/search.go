package search

import (
	"context"
	"fmt"
	"net/http"
	"net/url"

	"github.com/gin-gonic/gin"
)

// Handler aggregates all the interactions based on events
type Handler struct {
	controller Controller
	path string
}

// NewHandler creates a new Handler.
func NewHandler(controller Controller) Handler {
	return Handler{
		controller: controller,
		path: "https://dev-cern-search-ui.web.cern.ch/",
	}
}

// Controller wraps all the interactions with the param translator
type Controller interface {
	Translate(ctx context.Context, body map[string]string) map[string][]string
}

func (h Handler) RedirectSearchJson() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		var body map[string]string
		//var body PostParamTranslatorRequestBody
		if err := ctx.ShouldBindJSON(&body); err != nil {
			ctx.Redirect(http.StatusMovedPermanently, h.path)

			return
		}

		response := h.controller.Translate(
			ctx.Request.Context(),
			body,
		)

		params := url.Values{}
		for filter, values := range response {
			for _, value := range values {
				params.Add(filter, value)
			}
		}

		location := fmt.Sprintf("%v?%v", h.path, params.Encode())

		ctx.Redirect(http.StatusMovedPermanently, location)
	}
}


func (h Handler) RedirectSearch() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		if err := ctx.Request.ParseForm(); err != nil {
			ctx.Redirect(http.StatusMovedPermanently, h.path)

			return
		}

		body := make(map[string]string)
		simpleBindPostForm(&body, ctx.Request.PostForm)

		response := h.controller.Translate(
			ctx.Request.Context(),
			body,
		)

		params := url.Values{}
		for filter, values := range response {
			for _, value := range values {
				params.Add(filter, value)
			}
		}

		location := fmt.Sprintf("%v?%v", h.path, params.Encode())

		ctx.Redirect(http.StatusMovedPermanently, location)
	}
}

func simpleBindPostForm(body *map[string]string, form url.Values) {
	for k, v := range form {
		(*body)[k] = v[0]
	}
}
