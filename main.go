package main

import (
	"fmt"

	dependencies "github.com/carantunes/go-skeleton/api"
	"github.com/pkg/errors"
)

func main() {
	deps, err := dependencies.New()
	if err != nil {
		handlePanic(err, "error setting up dependencies")
	}

	if err := deps.API.Start(); err != nil {
		handlePanic(err, "error starting the API")
	}
}

func handlePanic(err error, errorMsg string) {
	fmt.Printf("Panic: %+v\n", err)

	panic(errors.Wrap(err, errorMsg))
}
