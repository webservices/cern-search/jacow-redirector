FROM golang:1.15.2-alpine

# update docker with latest patches
RUN apk upgrade --no-cache

# install required packages
RUN apk add --update --no-cache \
	make \
	curl

RUN curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(go env GOPATH)/bin v1.24.0

ENV WORKING_DIR=/app

RUN mkdir ${WORKING_DIR}
COPY . ${WORKING_DIR}
WORKDIR ${WORKING_DIR}
RUN go build -o build/main .

# Set folder permissions
ENV JACOW_USER_ID=1000
RUN chgrp -R 0 ${WORKING_DIR} && \
    chmod -R g=u ${WORKING_DIR}

RUN adduser -S -D -H -h /app -u ${JACOW_USER_ID} jacow && \
    chown -R jacow:root ${WORKING_DIR}

USER jacow

ENV CONFIG_PATH=api/config
ENV GOENV=prod

CMD ["./build/main"]