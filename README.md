# Jacow Redirector

## Install
### Requirements
* Go 1.15
* Add `${GOPATH//://bin:}/bin` to the PATH as mentioned in [go/wiki/GOPATH#integrating-gopath](https://github.com/golang/go/wiki/GOPATH#integrating-gopath))
* Install [golangci-lint](https://github.com/golangci/golangci-lint#binary)

## Recommended
* Setup file watcher with golangci-lint

## Dvelopment
- `make build-env` build the container
- `make shell-env` bash the container
- `make rebuild-env` rebuild the container
