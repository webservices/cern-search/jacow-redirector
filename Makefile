# Helper commands
SERVICE_NAME := jacow-redirector
DOCKER_COMPOSE_FILE := docker-compose.yml

lint:
	golangci-lint run
.PHONY:lint

api:
	go run *.go
.PHONY:api

DOCKER_COMPOSE := docker-compose -f $(DOCKER_COMPOSE_FILE)

build-env:
	$(DOCKER_COMPOSE) up --remove-orphans $(SERVICE_NAME)
.PHONY: build-env

rebuild-env:
	$(DOCKER_COMPOSE) up --build --remove-orphans $(SERVICE_NAME)
.PHONY: rebuild-env

shell-env:
	$(DOCKER_COMPOSE) run --service-ports $(SERVICE_NAME) /bin/sh
.PHONY: shell-env

