package domain

import "github.com/carantunes/go-skeleton/domain/paramtranslator"

// ControllerAggregate aggregates all the controllers to ease dependency injection
type ControllerAggregate struct {
	ParamTranslator paramtranslator.Controller
}