package paramtranslator

import (
	"context"
	"fmt"
	"strings"
)

// Controller aggregates interactions with the param translator
type Controller struct {
	ParamMappings []FilterMapping
	FilterMappings []FilterMapping
	DefaultFilters []string
}

// NewController returns an initialized source Controller
func NewController() Controller {
	return Controller{
		ParamMappings: []FilterMapping{
			FilterMapping{"textstr", "q"},
		},
		FilterMappings: []FilterMapping{
			FilterMapping{"author", "author_match"},
			FilterMapping{"title", "name_match"},
			FilterMapping{"keywords", "keyword_match"},
		},
		DefaultFilters: []string{
			"site:accelconf.web.cern.ch",
			"collection:PDF",
		},
	}
}

// GetDefaultCardRequest is the get default card request
type Request struct {
	UserID string
}

type FilterMapping struct {
	Legacy string
	New    string
}

// q=&f=url_match:ipac2018 ipac2017++title=banana
func (c Controller)  Translate(ctx context.Context, body map[string]string) map[string][]string {
	filters := make([]string, len(c.DefaultFilters))
	copy(filters, c.DefaultFilters)

	urlFilter := getURLFilters(body)
	if urlFilter != "" {
		filters = append(filters, urlFilter)
	}

	for _, filter := range c.FilterMappings {
		if value, ok := body[filter.Legacy]; ok {
			filters = append(filters, fmt.Sprintf("%s:%s", filter.New, value))
		}
	}

	params := make(map[string][]string)
	for _, filter := range filters {
		params["f"] = append(params["f"], filter)
	}

	for _, param := range c.ParamMappings {
		if value, ok := body[param.Legacy]; ok {
			params[param.New] = append(params[param.New], value)
		}
	}

	return params
}

func getURLFilters(body map[string]string) string {
	if body["allconf"] == "y" {
		return ""
	}

	var urlFilter []string
	for k, v := range body {
		if strings.HasPrefix(k, "conf") && v == "y" {
			urlFilter = append(urlFilter, strings.TrimPrefix(k, "conf"))
		}
	}

	if len(urlFilter) > 0 {
		return fmt.Sprintf("%s:%s", "url_match", strings.Join(urlFilter[:], " "))
	}

	return ""
}
