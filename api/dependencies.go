package dependencies

import (
	"os"

	"github.com/carantunes/go-skeleton/domain"
	"github.com/carantunes/go-skeleton/domain/paramtranslator"
	"github.com/carantunes/go-skeleton/gin"
	"github.com/carantunes/go-skeleton/viper"
	"github.com/pkg/errors"
)

// Dependencies holds exported dependencies
type Dependencies struct {
	API gin.APIService
}

// New resolves and returns initialized Dependencies
func New() (Dependencies, error) {
	cfg, err := viper.New(os.Getenv)
	if err != nil {
		return Dependencies{}, errors.WithStack(err)
	}

	api, err := gin.New(
		cfg.GetString("app.host"),
		cfg.GetString("app.port"),
		cfg.GetString("app.cors_domain"),
		cfg.GetString("app.mode"),
		newControllerAggregate(cfg),
	)
	if err != nil {
		return Dependencies{}, errors.WithStack(err)
	}

	return Dependencies{
		API: api,
	}, nil
}

func newControllerAggregate(cfg viper.ConfigService) domain.ControllerAggregate {
	return domain.ControllerAggregate{
		ParamTranslator: paramtranslator.NewController(),
	}
}
